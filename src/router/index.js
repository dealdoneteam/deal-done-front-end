import Vue from 'vue'

import Router from 'vue-router'
import index from '@/components/home'
import product from '@/components/product'
import company from '@/components/company'
import detailProduct from '@/components/detailProduct'
import searchResullt from '@/components/serchView'
import signIn from '@/components/signIn'
import signUp from '@/components/signUp'
import vendorStore from '@/components/vendorStore'
import Page404 from '@/components/Page404'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/product',
      name: 'product',
      component: product
    },
    {
      path: '/company/:id/product',
      name: 'company',
      component: company
    },
    {
      path: '/product/:id',
      name: 'detailProduct',
      component: detailProduct
    },
    {
      path: '/searchResullt',
      name: 'searchResullt',
      component: searchResullt
    },
    {
      path: '/signIn',
      name: 'signIn',
      component: signIn
    },
    {
      path: '/signUp',
      name: 'signUp',
      component: signUp
    },
    {
      path: '/vendorStore/:id',
      name: 'vendorStore',
      component: vendorStore
    },
    {
      path: '/Page404',
      name: 'Page404',
      component: Page404
    }

  ]
})
