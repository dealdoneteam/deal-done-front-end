import Api from '@/services/Api'

export default {
  getAll (endpoint) {
    return Api().get(endpoint)
  },
  save (endpoint, data) {
    return Api().post(endpoint, data)
  },
  update (endpoint, data) {
    return Api().put(endpoint + '/' + data.id, data)
  },
  delete (endpoint, id) {
    return Api().delete(endpoint + '/' + id)
  }
}
