import axios from 'axios'

export default () => {
  return axios.create({
    baseURL: 'http://173.212.243.78/deal_done/public/',
    withCredentials: false,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
}
