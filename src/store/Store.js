import Vue from 'vue'
import Vuex from 'vuex'

import productDetail from '../modules/productDetail'
import catgryDetail from '../modules/catgryDetail'
import vendorDetail from '../modules/VendorDetail'

Vue.use(Vuex)

export const store = new Vuex.Store({
  strict: true, // strict the changes outside mutations
  modules: {
    productDetail,
    catgryDetail,
    vendorDetail
  }
})
