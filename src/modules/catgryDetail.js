import ApiService from '../services/ApiService';

export default {
    strict: true,
    state:{
        selectedIndex: -1,
        categoryData:[],
        endPoint:'category'
    },
  
    mutations:{
        loadProducData(state, value) {
            state.categoryData = value;
            },
    },
    actions:{
        loadProducData({state,commit}) {
            if(state.categoryData.length==0){
            ApiService.getAll(state.endPoint)
            .then(res => {
                
                commit('loadProducData', res.data);
            });}
        
    }
}
}