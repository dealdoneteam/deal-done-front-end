import ApiService from '../services/ApiService';

export default {
    strict: true,
    state:{
        selectedIndex: -1,
        productData:[],
        endPoint:'product'
    },
  
    mutations:{
        loadProdData(state, value) {
            state.productData = value;
            },
    },
    actions:{
        loadProdData({state,commit}) {
            ApiService.getAll(state.endPoint)
            .then(res => {
                console.log(res.data);
                commit('loadProdData', res.data);
            });
        
    }
}
}