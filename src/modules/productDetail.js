import ApiService from '../services/ApiService'

export default {
  strict: true,
  state: {
    selectedIndex: -1,
    productData: [],
    productDataWith: [],
    companyDataWith: [],
    endPoint: 'product',
    endPointC: 'company',
    byVendorEndPoint: 'product/get_by_user/'

  },

  mutations: {
    loadProductData (state, value) {
      state.productData = value
    },

    loadProductDataWith (state, value) {
      state.productDataWith = value
    },

    loadCompanyDataWith (state, value) {
      state.companyDataWith = value
    }

  },
  actions: {

    loadProductData ({state, commit}) {
      if (state.productData.length === 0) {
        ApiService.getAll(state.endPoint)
          .then(res => {
            commit('loadProductData', res.data)
          })
      }
    },

    loadVendorProducts ({state, commit}, id) {
      return new Promise((resolve, reject) => {
        ApiService.getAll(state.byVendorEndPoint + id)
          .then(res => {
            resolve(res.data)
          })
      })
    },

    loadProductDataWith ({state, commit}, payload) {
      ApiService.getAll(state.endPoint + '/' + payload)
        .then(res => {
          commit('loadProductDataWith', res.data)
        })
    },

    loadCompanyDataWith ({state, commit}, payload) {
      ApiService.getAll(state.endPointC + '/' + payload + '/' + state.endPoint)
        .then(res => {
          commit('loadCompanyDataWith', res.data)
        })
    }

  }
}
