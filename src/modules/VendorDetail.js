import ApiService from '../services/ApiService'

export default {
  strict: true,
  state: {
    selectedIndex: -1,
    productData: [],
    productDataWith: [],
    companyDataWith: [],
    endPoint: 'user/'

  },

  mutations: {
    loadVendorData (state, value) {
      state.productData = value
    }
  },
  actions: {

    loadVendorData ({state, commit}) {
      if (state.productData.length === 0) {
        ApiService.getAll(state.endPoint)
          .then(res => {
            commit('loadVendorData', res.data)
          })
      }
    },

    loadCompany ({state, commit}, id) {
      return new Promise((resolve, reject) => {
        ApiService.getAll(state.endPoint + id + '/company')
          .then(res => {
            resolve(res.data)
          })
      })
    }
  }
}
